import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersist from 'vuex-persist'

Vue.use(Vuex)

const vuexPersist = new VuexPersist({
  key: 'my-sec-app',
  storage: localStorage
})

export default new Vuex.Store({
  plugins: [
    vuexPersist.plugin
  ],
  state: {
    usuario: null,
    token: null,
    autorizacoes:[],
    estaAutorizado:false
  },
  mutations: {
    setUsuario (state, usuario) {
      state.usuario = usuario
    },
    setToken (state, token)  {
      state.token = token
    },
    setEstaAutorizado (state,opcao){
      state.estaAutorizado = opcao
    },
    logout (state) {
      state.token = null
      state.usuario = null
      state.estaAutorizado = false
    },
    setAutorizacoes(state,autorizacoes){
      state.autorizacoes = autorizacoes
    },
    verificarAutorizacoes(state,usuario){
      this.autorizacoes = usuario.autorizacoes
      for(var i=0;i<this.autorizacoes.length; i++){
          if(this.autorizacoes[i].nome == 'ROLE_ADMIN'){
            state.estaAutorizado = true
          }else{
            state.estaAutorizado = false
          }
      }
    }
  },
  actions: {
      
  }
})
