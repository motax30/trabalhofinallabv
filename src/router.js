import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import store from './store'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/carro/getAll',
      name: 'carros',
      component: () => import('./views/CarroComponent.vue')
    },
    {
      path: '/cliente/getAll',
      name: 'clientes',
      component: () => import('./views/ClienteComponent.vue'),
      beforeEnter(to,from,next){
        if(store.state.usuario){
          next()
        }else{
          next(false)
          alert('Você não está autorizado a acessar o Cadastro de Clientes')
        }
      }
    },
    {
      path: '/Login',
      name: 'login',
      component: () => import('./views/Login.vue')
    }
  ]
})
